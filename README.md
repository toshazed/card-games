# Card Games

A project that will work thru some typical card games with a standard 52 deck of cards. The goal is to create a simple way to play more than 1 type of card game. A first step to create an turn based stategy card game, so it would be a good idea to design it w/ multiple card asset types (flexibility).

# Unity 3D Version

LTS Release 2018.4.10f1

# Get Started

Best to clone via ssh:

1. Create your key `ssh-keygen -t rsa -b 4096 -C "your_email@example.com"`
2. `ssh-add yourkey` for windows you might need to start up the ssh-agent with
    the `eval $(ssh-agent -s)` command beforehand. If this seems continual then 
    take a look at https://stackoverflow.com/questions/18404272/running-ssh-agent-when-starting-git-bash-on-windows
3. Take your public key and add it to the ssh keys in your gitlab profile.