﻿using UnityEditor;
using UnityEngine;

public static class CreateStandardCardAsset
{

    [MenuItem("Assets/Create/StandardCard")]
    public static void CreateYourScriptableObject()
    {
        ScriptableAbstractCardUnityIntegration.CreateAsset<StandardCard>();
    }

}
