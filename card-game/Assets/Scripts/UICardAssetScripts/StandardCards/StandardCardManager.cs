﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StandardCardManager : AbstractCardManager
{
    public StandardCardManager PreviewManager;

    [Header("Image References")]
    public Image CardGraphicImage;

    public override void Awake()
    {
        {
            if (cardAsset != null)
                ReadCardFromAsset();
        }
    }

    public override void ReadCardFromAsset()
    {

        if (cardAsset != null)
        {
            if(CardGraphicImage != null)
            {
                CardGraphicImage.sprite = cardAsset.CardImage;
            }
        }

        if (PreviewManager != null)
        {
            PreviewManager.cardAsset = cardAsset;
            PreviewManager.ReadCardFromAsset();
        }
    }
}
