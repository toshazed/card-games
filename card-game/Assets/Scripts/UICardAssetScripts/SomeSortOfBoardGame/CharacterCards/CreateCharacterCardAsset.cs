﻿using UnityEditor;
using UnityEngine;

public static class CreateCharacterCardAsset
{

    [MenuItem("Assets/Create/CharacterCard")]
    public static void CreateCharacterCardAssetScriptableObject()
    {
        ScriptableAbstractCardUnityIntegration.CreateAsset<CharacterCard>();
    }

}
