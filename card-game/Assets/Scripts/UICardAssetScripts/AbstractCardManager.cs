﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AbstractCardManager : MonoBehaviour
{
    protected bool CanBePlayedNow { get; set; }
    public AbstractCard cardAsset;
    public abstract void ReadCardFromAsset();
    public abstract void Awake();
    
}
