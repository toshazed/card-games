﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// How the abstract card class could get used.
/// The idea of the brawl card is to always be neutral but perhaps it could
/// give some characters extra perks. Might need to tie in some character info.
/// </summary>
public class CharacterCard : AbstractCard
{
    public enum TargetingOptions
    {
        NoTarget,
        AllCreatures,
        EnemyCreatures,
        YourCreatures,
        AllCharacters,
        EnemyCharacters,
        YourCharacters
    }

    [Header("Text Component References")]
    public Text NameText;
    public Text DescriptionText;

    [Header("Ability Info")]
    private int MaxHealth;
    private int Attack;
    private int AttacksPerTurn = 1;
    private bool Taunt;
    private bool Charge;
    private string CreatureScriptName;
    private int SpecialCreatureAmount;
    private TargetingOptions Targets;
}
