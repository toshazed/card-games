﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// How the abstract card class could get used.
/// The idea of the spell card is to always be neutral but maybe only some
/// characters could use it or some of it?
/// 
/// Might need to tie in some character info.
/// </summary>
public class SpellCard : AbstractCard
{
    public enum TargetingOptions
    {
        NoTarget,
        AllCreatures,
        EnemyCreatures,
        YourCreatures,
        AllCharacters,
        EnemyCharacters,
        YourCharacters
    }

    [Header("Text Component References")]
    public Text NameText;
    public Text DescriptionText;

    [Header("SpellInfo")]
    private string SpellScriptName;
    private int SpecialSpellAmount;
    private TargetingOptions Targets;
}
