﻿using UnityEngine;
using UnityEditor;

public static class ScriptableAbstractCardUnityIntegration
{
    /// <summary>
    /// Create a StandardCard asset in unity.
    /// </summary>
    /// <typeparam name="AbstractCard"></typeparam>
    public static void CreateAsset<AbstractCard>() where AbstractCard : ScriptableObject {
		var asset = ScriptableObject.CreateInstance<AbstractCard>();
		ProjectWindowUtil.CreateAsset(asset, "New " + typeof(AbstractCard).Name + ".asset");
	}
	
}