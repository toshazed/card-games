﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UntitleBoardGameCardManager : AbstractCardManager
{
    public UntitleBoardGameCardManager PreviewManager;

    [Header("Text Component References")]
    public Text NameText;
    public Text DescriptionText;

    [Header("Image References")]
    public Image CardGraphicImage;

    public override void Awake()
    {
        {
            if (cardAsset != null)
                ReadCardFromAsset();
        }
    }

    public override void ReadCardFromAsset()
    {

        if (cardAsset != null)
        {
            if (NameText != null && DescriptionText != null)
            {
                NameText.text = cardAsset.name;
                DescriptionText.text = cardAsset.Description;
            }

            if (CardGraphicImage != null)
            {
                CardGraphicImage.sprite = cardAsset.CardImage;
            }

        }

        if (PreviewManager != null)
        {
            PreviewManager.cardAsset = cardAsset;
            PreviewManager.ReadCardFromAsset();
        }
    }
}
