﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractDraggableActions : MonoBehaviour
{
    // The virtual keyword is used to modify a method, 
    // property, indexer, or event declaration and 
    // allow for it to be overridden in a derived class;
    public virtual bool CanDrag { get { return true;  } }

    public abstract void DragStart();
    public abstract void DragEnd();
    public abstract void Dragging();
    public abstract bool DragSuccess();
}
