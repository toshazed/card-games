﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardCardDrag : AbstractDraggableActions
{
    private StandardCardManager manager;


    public override bool CanDrag
    {
        get
        {
            return manager.CanBePlayedNow();
        }
    }

    public override void DragEnd()
    {
        throw new System.NotImplementedException();
    }

    public override void Dragging()
    {
        throw new System.NotImplementedException();
    }

    public override void DragStart()
    {
        throw new System.NotImplementedException();
    }

    public override bool DragSuccess()
    {
        throw new System.NotImplementedException();
    }
}
