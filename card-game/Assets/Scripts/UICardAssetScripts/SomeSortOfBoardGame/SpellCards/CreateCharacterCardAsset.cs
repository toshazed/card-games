﻿using UnityEditor;
using UnityEngine;

public static class CreateSpellCardAsset
{

    [MenuItem("Assets/Create/SpellCard")]
    public static void CreateCreateSpellCardAssetScriptableObject()
    {
        ScriptableAbstractCardUnityIntegration.CreateAsset<SpellCard>();
    }

}
