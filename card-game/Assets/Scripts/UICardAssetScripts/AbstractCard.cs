﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Abstract card is probably overkill right now but I think down the road
/// it could prove usefull. Might want to force sub-classes to implement
/// a less abstract method while keeping some global code intact.
/// </summary>
public abstract class AbstractCard : ScriptableObject
{
    [Header("General info")]
    public string Name;
    public string Description;
    public Sprite CardImage;


}
